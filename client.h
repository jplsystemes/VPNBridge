#ifndef CLIENT_H
#define CLIENT_H


#include <QDebug>
#include <QDateTime>
#include <QTcpSocket>
#include <QString>
#include <QProcess>
#include <QMessageBox>
#include <QThread>
#include <QStorageInfo>

class CoreClient;
class ConnectThread;

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QTcpSocket *vpnLauncherClient);
    void connect();
    void disconnect();
    void setPassphrase(std::string _passphrase);
	void setTapDriverMissing(bool _missing);
	bool isTapDriverMissing();
    ~Client();

public slots:
    void notifyConnected();
    void notifySessionEndedWithError();
    void notifySessionEndedOk();
    void notifyDisconnected();
	void notifyReconnecting();
    void reconnect();
    void promptForTapDriverInstall();

signals:
    void connected();
    void sessionEndedWithError();
    void sessionEndedOk();
    void disconnected();
	void reconnecting();
    void tapDriverMissing();

private:
    std::unique_ptr<std::thread> thread;
    CoreClient *coreClient;
    QTcpSocket *vpnLauncherClient;
    QProcess tapDriverInstaller;
    ConnectThread *connectThread;

    int nbTapDriverMissingWarnings;
    std::string passphrase;
	bool bIsTapDriverMissing;
};


#endif // CLIENT_H
