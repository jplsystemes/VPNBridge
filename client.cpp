#include "client.h"
#include <openvpn/common/platform.hpp>

#define OPENVPN_CORE_API_VISIBILITY_HIDDEN

#include <client/ovpncli.cpp>
#include <openvpn/io/io.hpp>
#include <openvpn/common/exception.hpp>
#include <openvpn/options/merge.hpp>

#include <QFileInfo>
#include <QDir>
#include <QCoreApplication>
#include <QDebug>

#ifdef __APPLE__
#include <security/Authorization.h>
#endif

#define MAX_RECONNECT_ATTEMPTS 3
#define USBSECUREKEYNAME "JPLSECURITY"

using namespace openvpn;

class CoreClient : public ClientAPI::OpenVPNClient
{

public:
    CoreClient(QTcpSocket *vpnLauncherClient, Client *client, std::string passphrase) :
            vpnLauncherClient(vpnLauncherClient),
            client(client),
            passphrase(passphrase)
    {
        qInstallMessageHandler(CoreClient::myMessageHandler);
    }

    bool parseConfiguration()
    {
        OptionList optionList();
        QString path = QFileInfo(QCoreApplication::applicationFilePath()).absoluteDir().path();
        QString confPath;

#ifdef Q_OS_WIN
         confPath = path.append("/config.conf");
#else
        QString securePartitionPath = getSecurePartitionPath();
        confPath = securePartitionPath.append("/VPNResources/config.conf");
#endif



        QFile file(confPath);
        if(!file.open(QFile::ReadOnly | QFile::Text))
            return false;


        ProfileMerge profileMerge(confPath.toStdString(),
                                                     "conf",
                                                     "",
                                                     ProfileMerge::FOLLOW_PARTIAL,
                                                     ProfileParseLimits::MAX_LINE_SIZE,
                                                     ProfileParseLimits::MAX_PROFILE_SIZE);
        if(profileMerge.status() != ProfileMerge::MERGE_SUCCESS)
            return false;

        config = new ClientAPI::Config();
        config->content = profileMerge.profile_content();
        config->privateKeyPassword = passphrase;
        config->connTimeout = 20;

        ClientAPI::EvalConfig eval = this->eval_config(*config);
        if(eval.error)
        {
            qDebug() << "Error" << QString::fromStdString(eval.message);
            return false;
        }

        return true;
    }

    static QString getSecurePartitionPath()
    {
        QList<QStorageInfo> drives = QStorageInfo::mountedVolumes();
        foreach (const QStorageInfo &storage, drives)
        {
            if (storage.isValid() && storage.isReady() && storage.displayName().contains(USBSECUREKEYNAME))
            {
                return storage.rootPath();
            }
        }

        return "";
    }

    static void myMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & msg)
    {
        QString txt;
        switch (type) {
        case QtDebugMsg:
            txt = QString("Debug: %1").arg(msg);
            break;
        case QtWarningMsg:
            txt = QString("Warning: %1").arg(msg);
            break;
        case QtCriticalMsg:
            txt = QString("Critical: %1").arg(msg);
            break;
        case QtFatalMsg:
            txt = QString("Fatal: %1").arg(msg);
            break;
        case QtInfoMsg:
            txt = QString("Info: %1").arg(msg);
            break;
        }
        QString logFilename = "";
#ifdef Q_OS_WIN
        logFilename = "log.txt";
        SetFileAttributesA(logFilename.toStdString().c_str(), FILE_ATTRIBUTE_HIDDEN);
#else
        logFilename = ".log";
#endif
        QFile outFile(getSecurePartitionPath().append(logFilename));
        qDebug() << getSecurePartitionPath().append("/" + logFilename);
        QFile outFile(getSecurePartitionPath().append("/" + logFilename));
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);
        ts << txt;
    }

    virtual bool socket_protect(int socket) override
    {
        qInfo().noquote() << "*** socket_protect" << QString::number(socket) << endl;
        return true;
    }

    virtual bool pause_on_connection_timeout() override
    {
        return false;
    }

    // Callback for delivering events during connect() call.
    // Will be called from the thread executing connect().
    virtual void event(const ClientAPI::Event& ev) override
    {
        qInfo().noquote() << QDateTime::currentDateTimeUtc().toString() << " EVENT: " << QString::fromStdString(ev.name);
        if (!ev.info.empty())
            qInfo().noquote() << " " << QString::fromStdString(ev.info);
		if (ev.fatal)
		{
			qInfo().noquote() << " [FATAL-ERR]";
			if (ev.name == "TUN_IFACE_CREATE")
				client->setTapDriverMissing(true);
		}
        else if (ev.error)
            qInfo().noquote() << " [ERR]";
        qInfo().noquote() << endl;

        if(ev.name == "CONNECTED")
        {
            emit client->connected();
        }
        else if(ev.name == "RECONNECTING")
        {
            if(nbReconnectAttempts >= MAX_RECONNECT_ATTEMPTS)
            {
                nbReconnectAttempts = 0;
                this->stop();
                emit client->disconnected();
            }
            else
            {
                nbReconnectAttempts += 1;
				emit client->reconnecting();
            }
        }
        else if(ev.name == "DISCONNECTED")
        {
            nbReconnectAttempts = 0;
            emit client->disconnected();
        }
    }

    // Callback for logging.
    // Will be called from the thread executing connect().
    virtual void log(const ClientAPI::LogInfo& info) override
    {
        // TODO: Write to logs on USB KEY
        qInfo().noquote() << QDateTime::currentDateTimeUtc().toString() << " " << QString::fromStdString(info.text);
    }

    // External PKI callbacks
    // Will be called from the thread executing connect().
    virtual void external_pki_cert_request(ClientAPI::ExternalPKICertRequest&) override
    {
        qDebug() << "external_pki_cert_request" << endl;

    }

    virtual void external_pki_sign_request(ClientAPI::ExternalPKISignRequest&) override
    {
        qDebug() << "external_pki_sign_request" << endl;
    }

    ~CoreClient()
    {

    }

private:

    ClientAPI::Config *config;
    QTcpSocket *vpnLauncherClient;
    Client *client;
    int nbReconnectAttempts = 0;
    std::string passphrase;
};


// Connect Thread
class ConnectThread : public QThread
{

public:
    ConnectThread(Client *client, CoreClient *coreClient, QTcpSocket *vpnLauncherClient) :
        client(client), coreClient(coreClient), vpnLauncherClient(vpnLauncherClient)
    {
    }

    void run() override
    {
        try
        {
            ClientAPI::Status connect_status = coreClient->connect();
            if(connect_status.error)
            {
                qInfo().noquote() << "Connection error: ";
                if(!connect_status.status.empty())
                    qInfo().noquote() << QString::fromStdString(connect_status.status) << ": ";
                qInfo().noquote() << QString::fromStdString(connect_status.message) << endl;
                emit client->sessionEndedWithError();
            }
			else if (client->isTapDriverMissing())
			{
				client->setTapDriverMissing(false);
#ifdef Q_OS_WIN
				emit client->tapDriverMissing();
#endif

            }
            else
            {
                qInfo().noquote() << QString::fromStdString(connect_status.message) << endl;
                emit client->sessionEndedOk(); // TODO: This gets called even tho network is unavailable
            }
        }
        catch(const std::exception& e)
        {
            qCritical() << QString::fromStdString(e.what());
            emit client->sessionEndedWithError();
        }
        qInfo().noquote() << "Session ended" << endl;
    }

    ~ConnectThread()
    {
        coreClient->stop();
    }

private:
    Client *client;
    CoreClient *coreClient;
    QTcpSocket *vpnLauncherClient;
};


// Client Implementation

Client::Client(QTcpSocket *vpnLauncherClient) :
    vpnLauncherClient(vpnLauncherClient),
    coreClient(nullptr),
    connectThread(nullptr),
    nbTapDriverMissingWarnings(0),
	bIsTapDriverMissing(false)
{
    QObject::connect(this, &Client::connected, this, &Client::notifyConnected);
    QObject::connect(this, &Client::sessionEndedWithError, this, &Client::notifySessionEndedWithError);
    QObject::connect(this, &Client::sessionEndedOk, this, &Client::notifySessionEndedOk);
    QObject::connect(this, &Client::reconnecting, this, &Client::notifyReconnecting);
    QObject::connect(this, &Client::disconnected, this, &Client::notifyDisconnected);

    QObject::connect(this, &Client::tapDriverMissing, this, &Client::promptForTapDriverInstall);

    QObject::connect(&tapDriverInstaller, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &Client::reconnect);
}


void Client::connect()
{

    coreClient = new CoreClient(vpnLauncherClient, this, passphrase);
    if(!coreClient->parseConfiguration())
    {
        vpnLauncherClient->write("missing_configuration_file\n");
        return;
    }

    if(connectThread != nullptr && connectThread->isRunning())
        connectThread->exit();

    connectThread = new ConnectThread(this, coreClient, vpnLauncherClient);
    connectThread->start();
}

void Client::disconnect()
{

    if(coreClient != nullptr)
        coreClient->stop();

    emit disconnected();
}

void Client::setPassphrase(std::string _passphrase)
{
    passphrase = _passphrase;
}

void Client::setTapDriverMissing(bool _missing)
{
	bIsTapDriverMissing = _missing;
}


bool Client::isTapDriverMissing()
{
	return bIsTapDriverMissing;
}

void Client::notifyConnected()
{
    vpnLauncherClient->write("connected\n");
}

void Client::notifySessionEndedOk()
{
    vpnLauncherClient->write("session_ended_ok\n");
}

void Client::notifySessionEndedWithError()
{
    vpnLauncherClient->write("session_ended_with_error\n");
}

void Client::notifyDisconnected()
{
    vpnLauncherClient->write("disconnected\n");
}

void Client::notifyReconnecting()
{
    vpnLauncherClient->write("reconnecting\n");
}

void Client::reconnect()
{
    this->connect();
}

void Client::promptForTapDriverInstall()
{
    vpnLauncherClient->write("session_ended_with_error_no_driver\n");

    if(nbTapDriverMissingWarnings >= 2)
        return;

    nbTapDriverMissingWarnings += 1;

    QMessageBox missingDriverMsgBox;
    missingDriverMsgBox.setIcon(QMessageBox::Information);
    missingDriverMsgBox.setWindowTitle("Pilote requis");
    missingDriverMsgBox.setText(tr("JPLSecurity nécessite l'installation d'un pilote afin de fonctionner, une boîte de dialogue vous demandant d'installer ce dernier va apparaître. Merci de cliquer sur Installer."));
    missingDriverMsgBox.setDefaultButton(QMessageBox::Ok);
    auto originalFlags = missingDriverMsgBox.windowFlags();
    missingDriverMsgBox.setWindowFlags(originalFlags | Qt::WindowStaysOnTopHint);
    missingDriverMsgBox.exec();

    QString tapDriverPath = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath() + "/tap-windows-driver/tap-windows-9.21.2.exe";

    if(!QFileInfo::exists(tapDriverPath))
    {
        QMessageBox missingDriverMsgBox;
        missingDriverMsgBox.setIcon(QMessageBox::Warning);
        missingDriverMsgBox.setWindowTitle("Pilote VPN manquant");
        missingDriverMsgBox.setText("Impossible de trouver le pilote TAP dans le chemin: " + tapDriverPath);
        missingDriverMsgBox.setDefaultButton(QMessageBox::Ok);
        auto originalFlags = missingDriverMsgBox.windowFlags();
        missingDriverMsgBox.setWindowFlags(originalFlags | Qt::WindowStaysOnTopHint);
        missingDriverMsgBox.exec();
        return;
    }
    tapDriverInstaller.start(tapDriverPath, QStringList("/S"));
}

Client::~Client()
{
    try
    {
        if(coreClient != nullptr)
        {
            coreClient->stop();
        }
    }
    catch(const std::exception& e)
    {
        qCritical().noquote() << QString::fromStdString(e.what()) << endl;
    }
}
