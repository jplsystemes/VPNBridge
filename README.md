# Presentation

VPNBridge is a tool which makes use of OpenVPN 3 Library in order to establish secure VPN connection to a remote server.

The project is gui-based and uses the Qt Framework for rendering its graphical interface.

# Building

Requirements:
* Qt 5.11.1 
* OpenVPN 3 Library (see [Building OpenVPN 3 Library](https://github.com/OpenVPN/openvpn3)


## OpenVPN3 dependencies location
The resulting library files (OpenVPN3 dependencies) from compiling OpenVPN3 Library are to be placed as following (Relative to project root):

### For Mac

src/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mac/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;asio-asio-1-12-0/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lz4-1.8.0/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mbedtls-2.7.0/  

These folders (under mac/) can be found in ~/src/mac (Following openvpn3 building instructions). 

### For Windows

src/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;windows/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;deps/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;amd64/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;asio/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jsoncpp/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lz4/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mbedtls/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tap-windows/  

### Common to all platforms

src/
    core/


OpenVPN3 source code is to be cloned under src/ folder as *core*.

These folders (under deps/) can be found in $O3/core/deps/ (Following openvpn3 building instructions)

Open <OpenVPN.pro> file into Qt Creator and build it. Output executables can be found in (depending on platform and configuration) :

- release/windows
- release/mac

- debug/windows
- debug/mac

# License

VPNBridge is licensed under [GNU Affero General Public License version 3](COPYRIGHT.AGPLV3) and [GNU Lesser General Public License version 3](COPYRIGHT.LGPLV3)
