#-------------------------------------------------
#
# Project created by QtCreator 2016-12-23T13:53:00
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    client.cpp

HEADERS  += mainwindow.h \
    client.h

win32-msvc* {
    QMAKE_CXXFLAGS += -MP
}

win32 {

    TARGET = vpn-bridge
    debug:DESTDIR = debug/windows
    release:DESTDIR = release/windows

    INCLUDEPATH += $${PWD}\src\core
    INCLUDEPATH += $${PWD}\src\windows\deps\x86\asio\asio\include
    INCLUDEPATH += $${PWD}\src\windows\deps\x86\mbedtls\include
    INCLUDEPATH += $${PWD}\src\windows\deps\x86\lz4\lib
    INCLUDEPATH += $${PWD}\src\windows\deps\x86\tap-windows\src

    LIBS += $${PWD}\src\windows\deps\x86\mbedtls\library\mbedtls.lib
    LIBS += $${PWD}\src\windows\deps\x86\lz4\lib\lz4.lib
    LIBS += -lfwpuclnt
    LIBS += -lws2_32
    LIBS += -lcrypt32
    LIBS += -liphlpapi
    LIBS += -lwinmm
    LIBS += -luser32
    LIBS += -lgdi32
    LIBS += -ladvapi32
    LIBS += -lwininet
    LIBS += -lshell32
    LIBS += -lole32
    LIBS += -lrpcrt4

    QMAKE_CXXFLAGS += /D_WIN32_WINNT=0x0600 /DNOMINMAX /D_CRT_SECURE_NO_WARNINGS /DUSE_ASIO /DASIO_STANDALONE /DASIO_NO_DEPRECATED /DUSE_MBEDTLS /DHAVE_LZ4 -DTAP_WIN_COMPONENT_ID=tap0901 /EHsc /MD /W0 /O2 /nologo
    QMAKE_CXXFLAGS -= -Zc:strictStrings

    RC_ICONS = "images/jplicon.ico"
}
macx {
    TARGET = com.jplsystemes.jplsecurity.VpnBridge
    CONFIG(debug, debug|release) {
        DESTDIR = debug/mac
    }

    CONFIG(release, debug|release) {
        DESTDIR = release/mac
    }

    INCLUDEPATH += $${PWD}/src/core
    INCLUDEPATH += $${PWD}/src/mac/mbedtls/mbedtls-osx/include
    INCLUDEPATH += $${PWD}/src/mac/asio/asio/include
    INCLUDEPATH += $${PWD}/src/mac/lz4/lz4-osx/include

    LIBS += -L$${PWD}/src/mac/mbedtls/mbedtls-osx/library -lmbedtls
    LIBS += $${PWD}/src/mac/lz4/lz4-osx/lib/liblz4.a

    QMAKE_LFLAGS += -v -framework Security -framework CoreFoundation -framework SystemConfiguration -framework IOKit -framework ApplicationServices
    QMAKE_CXXFLAGS += -fwhole-program -O3 -Wno-sign-compare -Wno-unused-parameter -arch x86_64 -std=c++11 -stdlib=libc++ -fvisibility=hidden -fvisibility-inlines-hidden -DUSE_MBEDTLS -DUSE_ASIO -DASIO_STANDALONE -DASIO_NO_DEPRECATED -DHAVE_LZ4 -w
    QMAKE_LFLAGS += -Xlinker -no_deduplicate -sectcreate __TEXT __info_plist vpn-bridge-Info.plist -sectcreate __TEXT __launchd_plist vpn-bridge-Launchd.plist
    CONFIG -= app_bundle
}

RESOURCES += \
    logojpl.qrc

FORMS +=

NB_COMMITS = $$system(git rev-list --count HEAD)
VERSION = 1.0.0.$$NB_COMMITS
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
