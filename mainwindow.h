#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QStandardPaths>
#include <QProcess>
#include <QDebug>
#include <QDir>
#include <QSettings>
#include <QDirIterator>
#include <QCommandLineParser>
#include <QMessageBox>
#include <QSocketNotifier>
#include <iostream>
#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>
#include <QSystemTrayIcon>
#include <thread>
#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QApplication &app, QWidget *parent = 0);
    void startLocalServer();
    ~MainWindow();

private slots:
    void newConnection();
    void readyRead();

private:
    Ui::MainWindow *ui;
    QProcess *process, *tseProcess;
    QString tseIp;
    QString subFolder;
    QStringList certsNames;
    QString appDataPath;
    QString secureKeyPath;
    QVector<QTcpSocket *> clients;
    QString getSecureKeyPath();
    QTcpServer *localServer;
    QSystemTrayIcon *trayIcon;
    QApplication *app;
    Client *client;

    void handleCommand(QString &command, QTcpSocket *client);
    void setupSystemTrayIcon();
    void connectClient(QTcpSocket *vpnLauncherClient);
    void disconnectClient(QTcpSocket *vpnLauncherClient);
};

#endif // MAINWINDOW_H
