#include "mainwindow.h"
#include <QApplication>

#ifdef Q_OS_MAC
#include <xpc/xpc.h>
#include <syslog.h>
#define HELPERNAME "com.jplsystemes.jplsecurity.VpnBridge"
#endif

int main(int argc, char *argv[])
{
    if(argc > 1 && strcmp(argv[1], "--version") == 0)
    {
        std::cout << QString(APP_VERSION).toStdString() << std::endl;
        return 0;
    }

    try
    {
        QApplication *a = new QApplication(argc, argv);
        QCoreApplication::setApplicationVersion(QString(APP_VERSION));


        qDebug() << "Connected";
        MainWindow w(*a);
        
        
#ifdef Q_OS_MAC
        dispatch_queue_t queue = dispatch_queue_create(HELPERNAME, 0);
        xpc_connection_t xpc_listener = xpc_connection_create_mach_service(HELPERNAME, queue, XPC_CONNECTION_MACH_SERVICE_LISTENER);

        xpc_connection_set_event_handler(xpc_listener, ^(xpc_object_t peer) {
            if ( xpc_get_type(peer) == XPC_TYPE_ERROR ) {
                syslog(LOG_ALERT, "listener error\n"); // XPC_ERROR_KEY_DESCRIPTION
            } else if ( xpc_get_type(peer) == XPC_TYPE_CONNECTION ) {
                syslog(LOG_ALERT, "listener connection %d\n", xpc_get_type(peer));
                xpc_connection_t connectionPeer = xpc_connection_t(peer);
                xpc_connection_set_event_handler(connectionPeer, ^(xpc_object_t msg) {
                    
                    syslog(LOG_ALERT, "XPC Type: %d", xpc_get_type(msg));
                    if (xpc_get_type(msg) == XPC_TYPE_DICTIONARY) {
                        syslog(LOG_ALERT, "XPC Dictionary received");
                        const char *command = xpc_dictionary_get_string(msg, "message");
                        syslog(LOG_ALERT, "requested %s", command);
                        syslog(LOG_ALERT, "XPC Dictionary processe5d");

                        syslog(LOG_ALERT, "pid %d requested %s", xpc_connection_get_pid(connectionPeer), command);
                        if(strcmp(command, "version") == 0)
                        {
                            const char *version = QCoreApplication::applicationVersion().toStdString().c_str();
                            xpc_object_t reply = xpc_dictionary_create_reply(msg);
                            xpc_connection_t connection = xpc_dictionary_get_remote_connection(msg);
                            xpc_dictionary_set_string(reply, "version", version);
                            xpc_connection_send_message(connection, reply);
                        }
                        else if(strcmp(command, "quit") == 0)
                        {
                            a->quit();
                        }
                        else if(strcmp(command, "start") == 0)
                        {
                            xpc_object_t reply = xpc_dictionary_create_reply(msg);
                            xpc_connection_t connection = xpc_dictionary_get_remote_connection(msg);
                            xpc_connection_send_message(connection, reply);
                        }
                    }
                });
                xpc_connection_resume(connectionPeer);
            } else {
                assert(false);
            }
        });
        
        xpc_connection_resume(xpc_listener);
#endif
        
        auto ret = a->exec();
        return ret;
    }
    catch(std::exception e)
    {
        qDebug() << e.what();
    }
}
