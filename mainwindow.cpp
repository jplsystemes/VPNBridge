#include "mainwindow.h"
#include "ui_mainwindow.h"

#ifdef Q_OS_MAC
#include <syslog.h>
#include <unistd.h>
#endif

MainWindow::MainWindow(QApplication &app, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    subFolder = "config/";;
    process = new QProcess(this);
    tseProcess = new QProcess(this);

    setupSystemTrayIcon();
    setWindowTitle("VPN Interface");

    startLocalServer();
    this->app = &app;

    app.setQuitOnLastWindowClosed(false);
}

void MainWindow::newConnection()
{
    qDebug() << QDateTime::currentDateTimeUtc() << ": New vpnLauncherClient";
    QTcpSocket *vpnLauncherClient = localServer->nextPendingConnection();
    clients.push_back(vpnLauncherClient);

    client = new Client(vpnLauncherClient);
    connect(vpnLauncherClient, &QTcpSocket::readyRead, this, &MainWindow::readyRead);
}

void MainWindow::readyRead()
{
    auto vpnLauncherClient = clients.last();

    QString command = "";
    while(!vpnLauncherClient->atEnd())
    {
        command = QString::fromUtf8(vpnLauncherClient->readLine());

        handleCommand(command, vpnLauncherClient);
    }
}

void MainWindow::startLocalServer()
{
    localServer = new QTcpServer();
    connect(localServer, &QTcpServer::newConnection, this, &MainWindow::newConnection);
    localServer->listen(QHostAddress::LocalHost, 52123);
}

void MainWindow::connectClient(QTcpSocket *vpnLauncherClient)
{
    client->connect();
}

void MainWindow::disconnectClient(QTcpSocket *vpnLauncherClient)
{
    client->disconnect();
}

void MainWindow::handleCommand(QString &command, QTcpSocket *vpnLauncherClient)
{
    command = command.simplified();
    if(command == "quit")
    {
//        trayIcon->showMessage("Déconnexion" , "Déconnexion en cours", QSystemTrayIcon::Information, 500);
        disconnectClient(vpnLauncherClient);
        app->exit();
    }
    else if(command.contains("passphrase"))
    {
        QString passphrase = command.mid(10);
        client->setPassphrase(passphrase.toStdString());
    }
    else if (command == "connect_vpn")
    {
//        trayIcon->showMessage("Connexion", "Connexion en cours", QSystemTrayIcon::Information, 500);
        connectClient(vpnLauncherClient);
    }
    else if(command == "disconnect_vpn")
    {
//        trayIcon->showMessage("Déconnexion" , "Déconnexion en cours", QSystemTrayIcon::Information, 500);
        disconnectClient(vpnLauncherClient);
    }
    else if(command.contains("key_path"))
    {
        secureKeyPath = command.split(" ")[1];
    }
}

void MainWindow::setupSystemTrayIcon()
{
    trayIcon = new QSystemTrayIcon(QIcon(":/images/images/jplicon.ico"));
    trayIcon->show();
}

MainWindow::~MainWindow()
{
    localServer->deleteLater();
    for(QTcpSocket *client: clients)
    {
        client->close();
        client->deleteLater();
    }
    delete ui;
}
